# business_card_layout

This Flutter project displays users business card details using simple widgets such as ImageView, Textview.
It uses Containers, Row and Column to stack information.

To see the same UI with different widgest please visit the branch 'using_card_tile' in the same repo.

 ![business_card](images/business_card.PNG)

